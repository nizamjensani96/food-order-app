import React, { useContext, useState } from 'react';

import classes from './Cart.module.css';
import Modal from '../UI/Modal';
import CartItem from './CartItem';
import CartContext from '../../store/cart-context';
import CheckoutForm from '../Checkout/CheckoutForm';

const Cart = (props) => {
  const cartCtx = useContext(CartContext);
  const [checkoutForm, setCheckoutForm] = useState(false);
  const [cartButtons, setCartButtons] = useState(true);
  const [orderIsSubmitted, setOrderIsSubmitted] = useState(false);
  const totalAmount = `RM${cartCtx.totalAmount.toFixed(2)}`;
  const hasItems = cartCtx.items.length > 0;

  const cartItemRemoveHandler = (id) => {
    cartCtx.removeItem(id);
  };

  const cartItemAddHandler = (item) => {
    cartCtx.addItem({ ...item, amount: 1 });
  };

  const cartItems = (
    <ul className={classes['cart-items']}>
      {cartCtx.items.map((item) => (
        <CartItem
          key={item.id}
          name={item.name}
          amount={item.amount}
          price={item.price}
          onRemove={cartItemRemoveHandler.bind(null, item.id)}
          onAdd={cartItemAddHandler.bind(null, item)}
        />
      ))}
    </ul>
  );
  const orderButtonHandler = (e) => {
    console.log('click');
    setCheckoutForm(true);
    setCartButtons(false);
  };

  const submitHandler = async (userData) => {
    await fetch(
      'https://react-meals-https-e4834-default-rtdb.asia-southeast1.firebasedatabase.app/orders.json',
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          user: userData,
          order: cartCtx.items,
        }),
      }
    );
    cartCtx.clearItem();
    setOrderIsSubmitted(true);
  };

  const cartSuccessOrderNotification = (
    <div>
      <p>Your orders has been successfully submitted!</p>
      <div className={classes.actions}>
        <button className={classes.button} onClick={props.onCloseCart}>
          Close
        </button>
      </div>
    </div>
  );

  const cartCloseOrderButtons = (
    <div className={classes.actions}>
      <button className={classes['button--alt']} onClick={props.onCloseCart}>
        Close
      </button>
      {hasItems && (
        <button className={classes.button} onClick={orderButtonHandler}>
          Order
        </button>
      )}
    </div>
  );
  const cartItemandTotal = (
    <div>
      <div className={classes.total}>
        <span>Total Amount</span>
        <span>{totalAmount}</span>
      </div>
      {checkoutForm && (
        <CheckoutForm
          onSubmit={submitHandler}
          onCancelSubmitting={props.onCloseCart}
        />
      )}
      {cartButtons && cartCloseOrderButtons}
    </div>
  );
  return (
    <Modal onClickingCloseCart={props.onCloseCart}>
      {!orderIsSubmitted && cartItems}
      {!orderIsSubmitted && cartItemandTotal}
      {orderIsSubmitted && cartSuccessOrderNotification}
    </Modal>
  );
};
export default Cart;
