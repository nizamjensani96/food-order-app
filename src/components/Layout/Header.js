import { Fragment } from 'react';
import mealsImage from '../../assets/meals.jpg';
import HeaderCardButton from './HeaderCartButton';
import classes from './Header.module.css';

const Header = (props) => {
  return (
    <Fragment>
      <header className={classes.header}>
        <h1>ReactMeals</h1>
        <HeaderCardButton clickToShowCart={props.onShowCart} />
      </header>
      <div className={classes['main-image']}>
        <img src={mealsImage} alt="A table food of delicious meals" />
      </div>
    </Fragment>
  );
};

export default Header;
