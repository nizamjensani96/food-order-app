import { useEffect, useState } from 'react';
import Card from '../UI/Card';
import classes from './AvailableMeals.module.css';
import MealItem from './MealItem/MealItem';
// const DUMMY_MEALS = [
//   {
//     id: 'm1',
//     name: 'Sushi',
//     description: 'Finest fish and veggies',
//     price: 22.99,
//   },
//   {
//     id: 'm2',
//     name: 'Schnitzel',
//     description: 'A german specialty!',
//     price: 16.5,
//   },
//   {
//     id: 'm3',
//     name: 'Barbecue Burger',
//     description: 'American, raw, meaty',
//     price: 12.99,
//   },
//   {
//     id: 'm4',
//     name: 'Green Bowl',
//     description: 'Healthy...and green...',
//     price: 18.99,
//   },
// ];

const AvailableMeals = () => {
  const [meals, setMeals] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [httpErrors, setHttpErrors] = useState(null);
  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch(
        'https://react-meals-https-e4834-default-rtdb.asia-southeast1.firebasedatabase.app/meals.json',
        {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
          },
        }
      );
      if (!response.ok) {
        throw new Error('Something is wrong!');
      }
      const data = await response.json();

      if (data === 0) {
        return;
      }

      const loadedMeals = [];
      for (const key in data) {
        loadedMeals.push({
          id: key,
          name: data[key].name,
          description: data[key].description,
          price: data[key].price,
        });
      }
      setMeals(loadedMeals);
      setIsLoading(false);
    };

    //fetchData is an async function, async function always return a promise,
    //if we throw an error inside a promise, that error will cause the promise to reject
    //the only way of handling an error inside a promise
    fetchData().catch((error) => {
      setIsLoading(false);
      setHttpErrors(error.message);
    });
  }, []);

  if (httpErrors) {
    return (
      <section className={classes.mealsError}>
        <p>{httpErrors}</p>
      </section>
    );
  }

  return (
    <section className={classes.meals}>
      <Card>
        {isLoading && (
          <p className={classes.mealsLoading}>Meals is Loading.....</p>
        )}
        {!isLoading && (
          <ul>
            {meals.map((meal) => (
              <MealItem
                key={meal.id}
                id={meal.id}
                name={meal.name}
                description={meal.description}
                price={meal.price}
              />
            ))}
          </ul>
        )}
      </Card>
    </section>
  );
};

export default AvailableMeals;
