import React, { useRef, useState } from 'react';
import classes from './CheckoutForm.module.css';

const isEmpty = (input) => input.trim().length === 0;
const emailIsValid = (email) => {
  return /\S+@\S+\.\S+/.test(email);
};
const telIsValid = (input) => input.trim().length >= 8;
const postcodeIsValid = (input) => input.trim().length === 5;

const CheckoutForm = (props) => {
  const [userInputIsValid, setUserInputIsValid] = useState({
    name: true,
    email: true,
    tel: true,
    address: true,
    postcode: true,
    city: true,
    state: true,
  });

  const nameInputRef = useRef();
  const emailInputRef = useRef();
  const telInputRef = useRef();
  const addressInputRef = useRef();
  const postcodeInputRef = useRef();
  const cityInputRef = useRef();
  const stateInputRef = useRef();

  const dataSubmitHandler = (e) => {
    e.preventDefault();

    const enteredName = nameInputRef.current.value;
    const enteredEmail = emailInputRef.current.value;
    const enteredTel = telInputRef.current.value;
    const enteredAddress = addressInputRef.current.value;
    const enteredPostcode = postcodeInputRef.current.value;
    const enteredCity = cityInputRef.current.value;
    const enteredState = stateInputRef.current.value;

    const enteredNameIsValid = !isEmpty(enteredName);
    const enteredEmailIsValid = emailIsValid(enteredEmail);
    const enteredTelIsValid = telIsValid(enteredTel);
    const enteredAddressIsValid = !isEmpty(enteredAddress);
    const enteredPostcodeIsValid = postcodeIsValid(enteredPostcode);
    const enteredCityIsValid = !isEmpty(enteredCity);
    const enteredStateIsValid = !isEmpty(enteredState);

    setUserInputIsValid({
      name: enteredNameIsValid,
      email: enteredEmailIsValid,
      tel: enteredTelIsValid,
      address: enteredAddressIsValid,
      postcode: enteredPostcodeIsValid,
      city: enteredCityIsValid,
      state: enteredStateIsValid,
    });

    const formIsValid =
      enteredNameIsValid &&
      enteredEmailIsValid &&
      enteredTelIsValid &&
      enteredAddressIsValid &&
      enteredPostcodeIsValid &&
      enteredCityIsValid &&
      enteredStateIsValid;

    if (!formIsValid) {
      return;
    }

    props.onSubmit({
      name: enteredName,
      email: enteredEmail,
      tel: enteredTel,
      address: enteredAddress,
      postcode: enteredPostcode,
      city: enteredPostcode,
      state: enteredState,
    });
  };
  return (
    <form className={classes.form} onSubmit={dataSubmitHandler}>
      <div
        className={`${classes.control} ${
          userInputIsValid.name ? '' : classes.invalid
        }`}
      >
        <label htmlFor="name">Name</label>
        <input id="name" type="text" ref={nameInputRef} />
        {!userInputIsValid.name && <p>Please enter a valid name!</p>}
      </div>

      <div
        className={`${classes.control} ${
          userInputIsValid.email ? '' : classes.invalid
        }`}
      >
        <label htmlFor="email">Email</label>
        <input id="email" type="email" ref={emailInputRef} />
        {!userInputIsValid.email && <p>Please enter a valid email!</p>}
      </div>
      <div
        className={`${classes.control} ${
          userInputIsValid.tel ? '' : classes.invalid
        }`}
      >
        <label htmlFor="tel">Phone Number</label>
        <input id="tel" type="tel" ref={telInputRef} />
        {!userInputIsValid.tel && <p>Please enter a valid phone number!</p>}
      </div>
      <div
        className={`${classes.control} ${
          userInputIsValid.address ? '' : classes.invalid
        }`}
      >
        <label htmlFor="address">Address</label>
        <input id="address" type="text" ref={addressInputRef} />
        {!userInputIsValid.address && <p>Please enter a valid address!</p>}
      </div>
      <div
        className={`${classes.control} ${
          userInputIsValid.postcode ? '' : classes.invalid
        }`}
      >
        <label htmlFor="postcode">Postcode</label>
        <input id="postcode" type="number" ref={postcodeInputRef} />
        {!userInputIsValid.postcode && <p>Please enter a valid postcode!</p>}
      </div>
      <div
        className={`${classes.control} ${
          userInputIsValid.city ? '' : classes.invalid
        }`}
      >
        <label htmlFor="city">City</label>
        <input id="city" type="text" ref={cityInputRef} />
        {!userInputIsValid.city && <p>Please enter a valid city!</p>}
      </div>
      <div
        className={`${classes.control} ${
          userInputIsValid.state ? '' : classes.invalid
        }`}
      >
        <label htmlFor="state">State</label>
        <input id="state" type="text" ref={stateInputRef} />
        {!userInputIsValid.state && <p>Please enter a valid state!</p>}
      </div>
      <div className={classes.actions}>
        <button onClick={props.onCancelSubmitting}>Cancel</button>
        <button className={classes.button} type="submit">
          Submit
        </button>
      </div>
    </form>
  );
};

export default CheckoutForm;
